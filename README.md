# Jekyll GitHub Showcase Template

jekyll-github-showcase-template is a customizable jekyll template that lets you display your GitHub repositories dynamically on your website.
You just have to enter your github username and this template generates a github projects showcase website for you (see screenshot below)

__Live view here:__ [LINK](https://jchristl.gitlab.io/jekyll-github-showcase-template/)

![Screenshot](screenshot.png "Example screenshot")

# How to set it up
## Step 1: Clone this repository
```sh
git clone git@gitlab.com:jchristl/jekyll-github-showcase-template.git
```
## Step 2: Set your Github username
Enter your username in ```fetch_github_data.sh```
## Step 3: Setup your CI/CD or load data manually
Your CI (Continuous integration) system or server should execute ```fetch_github_data.sh``` daily to pull new changes from your GitHub repositories.
A good way to do this is to use GitLab CI or GitHub Actions.
Alternatively, you can just pull them yourself and push them to your git repository after forking it:
```sh
chmod +x ./fetch_github_data && sh ./fetch_github_data.sh
git add ./_data/fetch/
git commit -m "Added github data"
git push
```
If you do it that way, you always have to execute it manually once you add a new repository.
### Easy way if you host your page via GitLab Pages:
Just use my .gitlab-ci.yml file and let GitLab CI do the rest.
## Step 4: Customize it how you want
Customize the number of shown repositories in ```_config.yml``` and change the colors how you want them in ```_sass/jekyll-github-showcase-template.scss```
## Step 5: Run it!
You can run it like any other jekyll template:
```sh
bundle exec jekyll serve
```

# TL;DR: Running it locally
```sh
git clone <repo url>
cd jekyll-github-showcase-template
# Open fetch_github_data.sh and change USERNAME to your github username
sh ./fetch_github_data.sh # to fetch needed github data files
bundle exec jekyll serve
```
