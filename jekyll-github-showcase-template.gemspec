# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "jekyll-github-projects-view"
  spec.version       = "0.1.0"
  spec.authors       = ["jchristl"]
  spec.email         = ["christl.joshua@googlemail.com"]

  spec.summary       = "customizable jekyll template that lets you display your GitHub repositories dynamically on your website"
  spec.homepage      = "https://gitlab.com/jchristl/jekyll-github-projects-view.gitlab.io"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.0"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 12.0"
end
